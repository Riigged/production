﻿#region

using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;

#endregion

namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ Janus = () => Behav()
        .Init("md Janus the Doorwarden",
            new State(
                new HpLessTransition(0.10, "Death"),
                new State("PreEverything",
                    new EntityNotExistsTransition("Suit of Armor", 666, "Idle")
                    ),
                new State("Death",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new Taunt("You have defeated me, well done champion!"),
                    new TimedTransition(2500, "Suicide")
                    ),
                new State("Suicide",
                new TransformOnDeath("md Loot Balloon Doorwarden"),
                    new Order(500, "Md LightKey", "Suicide"),
                    new Order(500, "Md LightKey1", "Suicide"),
                    new Order(500, "Md LightKey2", "Suicide"),
                    new Order(500, "Md LightKey3", "Suicide"),
                    new Order(500, "Md DarkKey", "Suicide"),
                    new Order(500, "Md DarkKey1", "Suicide"),
                    new Order(500, "Md DarkKey2", "Suicide"),
                    new Order(500, "Md DarkKey3", "Suicide"),
                    new Suicide()
                    ),
                new State("Idle",
                    new ConditionalEffect(ConditionEffectIndex.Invincible),
                    new Taunt("You trash His Lordshop's castle, kill his brothers, and challenge us. Come if you dare."),
                    new PlayerWithinTransition(10, "Begin")
                    ),
                new State("Begin",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new Taunt("Keys! Defend me!"),
                    new TimedTransition(3000, "Toss")
                    ),
                new State("Toss",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new TossObject("md LightKey2", 10, 45, coolDown: 3000, randomToss: false),
                    new TossObject("md DarkKey1", 8, 90, coolDown: 3000, randomToss: false),
                    new TossObject("md LightKey1", 10, 135, coolDown: 3000, randomToss: false),
                    new TossObject("md Darkkey2", 8, 180, coolDown: 3000, randomToss: false),
                    new TossObject("md LightKey3", 10, 225, coolDown: 3000, randomToss: false),
                    new TossObject("md DarkKey", 8, 270, coolDown: 3000, randomToss: false),
                    new TossObject("md LightKey", 10, 315, coolDown: 3000, randomToss: false),
                    new TossObject("md DarkKey3", 8, 360, coolDown: 3000, randomToss: false),
                    new TimedTransition(2000, "Start")
                    ),
                new State("Start",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new Shoot(8, 10, projectileIndex: 0, coolDown: 50),
                    new TimedTransition(5000, "ComeCloser")
                    ),
                new State("ComeCloser",
                    new Shoot(8, 10, projectileIndex: 0, coolDown: 50),
                    new Taunt("Keys! Defend me!"),
                    new Order(500, "md LightKey", "GoToMaster"),
                    new Order(500, "md LightKey1", "GoToMaster"),
                    new Order(500, "md LightKey2", "GoToMaster"),
                    new Order(500, "md LightKey3", "GoToMaster"),
                    new Order(500, "md DarkKey", "GoToMaster"),
                    new Order(500, "md DarkKey1", "GoToMaster"),
                    new Order(500, "md DarkKey2", "GoToMaster"),
                    new Order(500, "md DarkKey3", "GoToMaster"),
                    new TimedTransition(1, "MoreShooting")
                    ),
                new State("MoreShooting",
                    new Shoot(8, 10, projectileIndex: 0, coolDown: 50),
                    new TimedTransition(6000, "GoAway")
                    ),
                    new State("GoAway",
                    new Shoot(8, 10, projectileIndex: 0, coolDown: 50),
                    new Order(500, "Md LightKey", "GoAwayFromMaster"),
                    new Order(500, "Md LightKey1", "GoAwayFromMaster"),
                    new Order(500, "Md LightKey2", "GoAwayFromMaster"),
                    new Order(500, "Md LightKey3", "GoAwayFromMaster"),
                    new Order(500, "Md DarkKey", "GoAwayFromMaster"),
                    new Order(500, "Md DarkKey1", "GoAwayFromMaster"),
                    new Order(500, "Md DarkKey2", "GoAwayFromMaster"),
                    new Order(500, "Md DarkKey3", "GoAwayFromMaster"),
                    new TimedTransition(1, "PreHourglass2")
                    ),
                new State("PreHourglass2",
                    new Shoot(8, 10, projectileIndex: 0, coolDown: 50),
                    new TimedTransition(1500, "PreHourglass")
                    ),
                new State("PreHourglass",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new Taunt("I will now shoot like an hourglass! Watch your steps!"),
                    new Order(500, "Md LightKey", "Suicide"),
                    new Order(500, "Md LightKey1", "Suicide"),
                    new Order(500, "Md LightKey2", "Suicide"),
                    new Order(500, "Md LightKey3", "Suicide"),
                    new Order(500, "Md DarkKey", "Suicide"),
                    new Order(500, "Md DarkKey1", "Suicide"),
                    new Order(500, "Md DarkKey2", "Suicide"),
                    new Order(500, "Md DarkKey3", "Suicide"),
                    new TimedTransition(2000, "shootup")
                    ),
                new State("shootup",
                        new Shoot(10, count: 8, fixedAngle: 0, shootAngle: 6, projectileIndex: 1, coolDown: 50),
                        new Shoot(10, count: 8, fixedAngle: 180, shootAngle: 6, projectileIndex: 1, coolDown: 50),
                    new TimedTransition(6000, "Pause1")
                    ),
                new State("shootsides",
                        new Shoot(10, count: 8, fixedAngle: 90, shootAngle: 6, projectileIndex: 1, coolDown: 50),
                        new Shoot(10, count: 8, fixedAngle: 270, shootAngle: 6, projectileIndex: 1, coolDown: 50),
                    new TimedTransition(6000, "Pause2")
                    ),
                new State("Pause1",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new TimedTransition(3000, "shootsides")
                    ),
                new State("Pause2",
                    new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                    new TimedTransition(3000, "Begin")
                )))
        .Init("md LightKey",
            new State(
                new State("standstillshit",
                    new Shoot(500, count: 15, projectileIndex: 0, coolDown: 800)
                    ),
                new State("GoToMaster",
                    new MoveTo(-5, 5, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("GoAwayFromMaster",
                    new MoveTo(5, -5, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("Suicide",
                    new Suicide()
                    )
                )
            )
        .Init("md LightKey1",
            new State(
                new State("standstillshit",
                    new Shoot(500, count: 15, projectileIndex: 0, coolDown: 800)
                    ),
                new State("GoToMaster",
                    new MoveTo(5, -5, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("GoAwayFromMaster",
                    new MoveTo(-5, 5, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("Suicide",
                    new Suicide()
                    )
                )
            )
        .Init("md LightKey2",
            new State(
                new State("standstillshit",
                    new Shoot(500, count: 15, projectileIndex: 0, coolDown: 800)
                    ),
                new State("GoToMaster",
                    new MoveTo(-5, -5, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("GoAwayFromMaster",
                    new MoveTo(5, 5, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("Suicide",
                    new Suicide()
                    )
                )
            )
        .Init("md LightKey3",
            new State(
                new State("standstillshit",
                    new Shoot(500, count: 15, projectileIndex: 0, coolDown: 800)
                    ),
                new State("GoToMaster",
                    new MoveTo(5, 5, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("GoAwayFromMaster",
                    new MoveTo(-5, -5, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("Suicide",
                    new Suicide()
                    )
                )
            )
        .Init("md DarkKey",
            new State(
                new State("standstillshit",
                    new Shoot(500, count: 15, projectileIndex: 0, coolDown: 800)
                    ),
                new State("GoToMaster",
                    new MoveTo(0, 6, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("GoAwayFromMaster",
                    new MoveTo(0, -6, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("Suicide",
                    new Suicide()
                    )
                )
            )
        .Init("md DarkKey1",
            new State(
                new State("standstillshit",
                    new Shoot(500, count: 15, projectileIndex: 0, coolDown: 800)
                    ),
                new State("GoToMaster",
                    new MoveTo(0, -6, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("GoAwayFromMaster",
                    new MoveTo(0, 6, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("Suicide",
                    new Suicide()
                    )
                )
            )
        .Init("md DarkKey2",
            new State(
                new State("standstillshit",
                    new Shoot(500, count: 15, projectileIndex: 0, coolDown: 800)
                    ),
                new State("GoToMaster",
                    new MoveTo(6, 0, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("GoAwayFromMaster",
                    new MoveTo(-6, 0, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("Suicide",
                    new Suicide()
                    )
                )
            )
        .Init("md DarkKey3",
            new State(
                new State("standstillshit",
                    new Shoot(500, count: 15, projectileIndex: 0, coolDown: 800)
                    ),
                new State("GoToMaster",
                    new MoveTo(-6, 0, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("GoAwayFromMaster",
                    new MoveTo(6, 0, speed: 0.5),
                    new TimedTransition(2000, "standstillshit")
                    ),
                new State("Suicide",
                    new Suicide()
                    )
                )
            )
        .Init("md dwspawner",
            new State(
                new ConditionalEffect(ConditionEffectIndex.Invincible)
                )
            )
        .Init("md Loot Balloon Doorwarden",
                new State(
                    new State("Idle",
                        new ConditionalEffect(ConditionEffectIndex.Invulnerable),
                        new TimedTransition(5000, "Janus")
                    ),
                    new State("Janus")
                ),
                new Threshold(0.5,
                    new TierLoot(11, ItemType.Weapon, 0.3),
                    new TierLoot(12, ItemType.Weapon, 0.2),
                    new TierLoot(6, ItemType.Ability, 0.2),
                    new TierLoot(12, ItemType.Armor, 0.2),
                    new TierLoot(13, ItemType.Armor, 0.1),
                    new TierLoot(6, ItemType.Ring, 0.2),
                    new ItemLoot("Potion of Vitality", 1)
                    )
                )
            ;
    }
}